/*
Hentai or Normal 

NOTE: 'hentai or normal' could be it's own game by utilizing the boolean isAdult parameter
    from https://anilist.github.io/ApiV2-GraphQL-Docs/media.doc.html.
    But a hard coded game will be a Lot easier, at least for the time being.
                                                            ___
||      ||  ||======   //      ||    =======    =========   \_/
||      ||  ||         ||\\    //      ||       ||      ||  ||
||=====||   ||====    ||  \\  //       ||       ||===== ||  ||
||     ||   ||        ||   \\//        ||       ||      ||  ||
||     ||   ||=====   ||    //         ||       ||      ||  ||

Author: a dwarf named Nils (_sonsan)
*/
const honQuiz = require('../data/hentai.json');

module.exports = {
    name: 'hon',
    description: 'guess if a anime is hentai or normal based on the description',
    exec(msg) {
        const item = honQuiz[Math.floor(Math.random() * honQuiz.length)]
        msg.channel.send(item.description).then(() => {
            msg.channel.awaitMessages(r => item.answer === r.content.toLowerCase(), {maxMatches: 1, time: 10000, errors: ['time']})
                .then(a => msg.channel.send(`${a.first().author} guessed right\n${item.link}`))
                .catch(a => msg.channel.send(`\n\nno one guessed the right answer\nIt would have been ${item.link}`))
        });
    },
}
