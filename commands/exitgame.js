const { gcActive, gcSetActive, gcSetInactive } = require('../handlers/dbHandler');

module.exports = {
    name: 'exitgame',
    description: `either guess a Series based on a Character picture AND name
                or guess the Series or the Character based on the picture alone`,
    args: true,
    async exec(msg, args, server) {
        // NOTE: I could place this in a file named gameHandler together with other commands.
       if (await gcActive(msg)) {
            await gcSetInactive(msg);
            msg.react('✅');
        }
       else {
            msg.channel.send(`***no game in progress.***`);
       }
    }
}