const api = require('../handlers/apiHandler');
const charQuery = `query($search : String) { Character (search: $search) { siteUrl, image { medium, large }, name { first, last }}}`;
const seriesQuery = `query($search : String, $type : MediaType) { Media ( search : $search, type : $type ) { siteUrl } }`;

const char = async name => {
    const response = await api(charQuery, name);
    return response.Character.siteUrl;
};

const series = async title => {
    const response = await api(seriesQuery, title);
    return response.Media.siteUrl;
};

module.exports = {
    name : 'search',
    aliases: ['s', 'c'],
    usage: 'foo',
    description : 'query anilistAPI for the caracter profile',
    async exec(msg, args, servers) {
        if (args[0] === 'c') {
            msg.channel.send(await char({search: args.join(' ')}));
        } else {
            msg.channel.send(await series({search: args.join(' ')}))
        }
    }
}