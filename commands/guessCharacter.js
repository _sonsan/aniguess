/* source(s):
*   /_ https://anilist.github.io/ApiV2-GraphQL-Docs/query.doc.html
*   /_ https://anilist.gitbook.io/anilist-apiv2-docs/overview/graphql/getting-started
*/
const { RichEmbed } = require('discord.js');
const { gcActive, gcSetActive, gcSetInactive } = require('../handlers/dbHandler');
const { logger } = require('../handlers/logHandler');
const api = require('../handlers/apiHandler');

// TODO: add synonyms/nicknames
const query = `query($userName : String, $type : MediaType, $status : MediaListStatus) { MediaListCollection(userName : $userName, type : $type, status : $status) { lists { entries { media { id, title {romaji, english}, characters (role : MAIN) { nodes { name { first, last }, image { large, medium } } } } } } } }`

const fetchCharacters = async arg => {
    const response = await api(query, arg);
    if (!response) return 404;
    const data = response.MediaListCollection.lists[0].entries;

    let animes = [];
    for (let entry of data) {
        if (!entry.media.characters['nodes'].length) continue;
        let anime = {id: entry.media.id, name_ro: entry.media.title.romanji || '1337', name_en: entry.media.title.english || '1337', chars: []};
        for (let char of entry.media.characters['nodes']) {
            anime.chars.push(char);
        }
        animes.push(anime);
    }
    return animes
};

const intersection = async (A, B) => {
    let inter = [];
    for (let i = 0; i < A.length; i++) {
        for (let j = 0; j < B.length; j++) A[i].id === B[j].id ? inter.push(A[i]) : null;
    }
    return inter;
};

async function game(msg, server, round, rounds, gT) {
    const series = server.gcPlayers[0][Math.floor(Math.random()*server.gcPlayers[0].length)];
    const char = series.chars[Math.floor(Math.random()*series.chars.length)];

    const s_ro = series.name_ro.toLowerCase();
    const s_en = series.name_en.toLowerCase();
    const f_name = char.name.first === null || char.name.first === '' ? '1337420' : char.name.first.toLowerCase();
    const l_name = char.name.last === null || char.name.last === '' ? '1337420' : char.name.last.toLowerCase();

    const filter = r => s_en === r.content.toLowerCase() || s_ro === r.content.toLowerCase()
        || f_name === r.content.toLowerCase() || l_name === r.content.toLowerCase()
        || (`${f_name} ${l_name}` || `${l_name} ${f_name}`) === r.content.toLowerCase()
        || (s_ro.replace(/[^a-zA-Z0-9]/g, '') || s_en.replace(/[^a-zA-Z0-9]/g, '')) === r.content.toLowerCase()
    
    const embed = new RichEmbed().setColor(0x610C05).setTitle(`Round ${round}/${rounds}`).setImage(char.image.large).setFooter('tip:');
    msg.channel.send(embed);
    
    /*
    * Collect all messages which abide the filter,
    * then start the next round if a correct answer and no stop command was provided.
    */
    msg.channel.awaitMessages(filter, {maxMatches : 1, time : gT*1000})
        .then(async c => {
            if (await gcActive(msg)) c.first().reply('knew the right answer.');
            next_round();})
        .catch(async e => {
            if (await gcActive(msg)) msg.channel.send('no one guessed the right answer :3')
            next_round();});
    
    const next_round = async () => {
        if (round < rounds && await gcActive(msg)) {
            game(msg, server, round + 1, rounds, gT);
        } else {
            // only send 'game over' message if game ended normally
            if (await gcActive(msg)) msg.channel.send(`***game is over.***`); 
            gcSetInactive(msg);
        }};
}

module.exports = {
    name: 'gc',
    description: `either guess a Series based on a Character picture AND name
                or guess the Series or the Character based on the picture alone`,
    args: true,
    async exec(msg, args, servers) {
        /*
        * Only start the game if no other 'gc' command is active in the guild
        */
        if (!await gcActive(msg)) {
            await gcSetActive(msg);
            servers[msg.guild.id] = {gcPlayers: []};    // WORKAROUND maybe fix in future
            let guessTime, rounds;
            if (args.length === 2) {
                guessTime = parseInt(args[1]) || 20;
                rounds = parseInt(args[0]) || 5;
                msg.channel.send(`no AniList profile was provided,\ndefaulting to **Sonsan** (will use the last three seasons in the future)`)
                servers[msg.guild.id].gcPlayers.push(await fetchCharacters({userName: 'Sonsan', type: 'ANIME', status: 'COMPLETED'}));
            } else if (args.length > 4) {
                msg.channel.send('you provided too many arguments');
                return;
            } else {
                guessTime = parseInt(args[args.length - 1]) || 20;
                rounds = parseInt(args[args.length - 2]) || 5;
                
                logger.info(`beginning character fetch in ${msg.guild.id}`);
                const p1 = await fetchCharacters({userName : args[0], type : 'ANIME', status : 'COMPLETED'});
                const p2 = await fetchCharacters({userName : args[1], type : 'ANIME', status : 'COMPLETED'});
                logger.info(`completed character fetch in ${msg.guild.id}`);

                if (p1 === 404 || p2 === 404) {
                    msg.channel.send(`AniList returned ***404*** for **${args[0]}** or **${args[1]}** \n**cannot start game**`);
                    return;
                } else {
                    msg.channel.send(`starting **${rounds} rounds** with **${guessTime} guess time** on: ${args[0]} ∩ ${args[1]}.`)
                    servers[msg.guild.id].gcPlayers.push(await intersection(p1, p2));
                }
            }
            game(msg, servers[msg.guild.id], 1, rounds, guessTime).catch(e => console.log(e));
        } else {
            msg.channel.send('***game in progress***');
        }
    } 
}

