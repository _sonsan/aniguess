/*
* Main source(s):
*    /_ https://discord.js.org/#/docs/main/stable/general/welcome
*    /_ https://discordjs.guide/
*
*                           __
*||||||||||     ||    ||   \__/ 
*||      ||    ||\\   ||    ||
*||======||   ||  \\  ||    ||
*||      ||   ||   \\//     ||
*||      ||  ||     //      || 
*Author: a dwarf named Nils (_sonsan)
*/
const fs = require('fs');
const Discord = require('discord.js');
const config = require('./config.json');
const { Server, addServer, userJoin, userLeave } = require('./handlers/dbHandler')
const { logger } = require('./handlers/logHandler');

const PREFIX = config.prefix;
const bot = new Discord.Client();

// map all commands from the commands folder into bot.commands 
bot.commands = new Discord.Collection();
const cF = fs.readdirSync('./commands').filter(f => f.endsWith('.js'));
for (const file of cF) {
    const command = require(`./commands/${file}`);
    bot.commands.set(command.name, command);
}

bot.once('ready', () => {
    logger.info(`1337 to 420 all day long BABY!`);
    bot.user.setActivity(`${PREFIX}help`);
    Server.sync();
});

bot.on('error', e => {
    logger.error(`${e}`);
});

bot.on('guildMemberAdd', member => {
    userJoin(member);
    logger.info(`a user joined ${member.guild.id}`);
});

bot.on('guildMemberRemove', member => {
    userLeave(member);
    logger.info(`a user left ${member.guild.id}`);
});

bot.on('guildCreate', async guild => {
    // add nececarry information about the server to the DB once the bot joins a guild
    try {
        addServer(guild);
        logger.info(`added guild: ${guild.id} with ${guild.memberCount} member's to database.`);
    } catch (e) {
        logger.info(`failed to add guild ${guild.id} to database with ${e}.`);
    }
});

// contains temporary infomation i.e.: song queue, character queue or dispatcher information
let servers = {};
bot.on('message', async msg => {
    const args = msg.content.slice(PREFIX.length).split(/ +/);
    const commandName = args.shift().toLowerCase();
    if (msg.author.bot || !msg.content.startsWith(PREFIX)) return;
    if (msg.channel.type === 'dm' || !bot.commands.has(commandName)) return;
    const command = bot.commands.get(commandName);
    try {
        command.exec(msg, args, servers);
    } catch (e) {
        logger.warn(`execution of command: ${commandName} failed in guild ${msg.guild.id} with ${e}`)
    }
});

process.on('uncaughtException', e => logger.error(e));

bot.login(config.token);